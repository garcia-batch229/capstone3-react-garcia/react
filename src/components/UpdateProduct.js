
import {useState, useEffect, useContext} from 'react';
import { Form, Button } from 'react-bootstrap';
import {useParams,  useNavigate, Link} from 'react-router-dom';
import UserContext from '../UserContext.js';
import Swal from 'sweetalert2'


export default function UpdateProduct() {
 	
	const navigate = useNavigate();
	
	const {user} = useContext(UserContext);

	const {productId} = useParams();

 
	const [name, setName] = useState("");
	const [description, setDescription] = useState(String);
	const [price, setPrice] = useState(Number);
	const [isActive, setIsActive] = useState(false);


	useEffect(() => {
		if(name !== '' && description !== '' && price!== ''){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	},[name, description, price])


	useEffect(() => {
		

		fetch(`${ process.env.REACT_APP_API_URL }/products/${productId}`)
		.then(res => res.json())
		.then(data => {

		console.log(data);

		setName(data.productName);
		setDescription(data.description);
		setPrice(data.price);

		});
	}, [])




	
	function updateProduct(e){
		e.preventDefault();


		fetch(`${ process.env.REACT_APP_API_URL }/products/${productId}`, {
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
				
			},
			body: JSON.stringify({
				productName: name,
				description: description,
				price: price
			})

		})
		.then(res => res.json())
		.then(data => {
			if(data){
				Swal.fire({
                    title: "Product Successfully Updated!",
                    icon: "success",
                    text: "You may now check on product list"
                });
                setName('');
                setDescription('');
                setPrice('');
                navigate('/admin');

			}
			else{
				Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Please try again"
                });
			}
		})	
	}
		



	


	return (

		<>
		<Form className="text-light" onSubmit={e => updateProduct(e)}>
            <h1 className="my-5 text-center text-light">Update Product</h1>
            <Form.Group className="mb-3" controlId="pName">
                <Form.Label>Product Name</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Enter product name"
                    value={name}
                    onChange={e => setName(e.target.value)}
                    
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="productDescription">
                <Form.Label>Description</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Enter description"
                    value={description}
                    onChange={e => setDescription(e.target.value)}
                    
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="price">
                <Form.Label>Price</Form.Label>
                <Form.Control
                    type="number"
                    placeholder="Enter price"
                    onChange={e => setPrice(e.target.value)}
                    value={price}
                    
                />
                
            </Form.Group>

            {
                isActive
                ?
                    <Button variant="primary" type="submit" id="submitBtn">
                    Update
                    </Button>

                :
                    <Link className="btn btn-primary" to={`/admin`}>Return</Link>
            }
        </Form>
		
        </>
		)
}