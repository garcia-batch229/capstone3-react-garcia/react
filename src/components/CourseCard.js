import {Card,Button} from 'react-bootstrap';
import {useState} from 'react'
import {Link} from 'react-router-dom'
 
export default function CourseCard({courseProps}) {
 
	//Checks to see if the data was successfully passed
	//console.log(courseProps);
	//console.log(typeof courseProps);

	//Destructuring the data to avoid dot notation
	const {productName, description, price, isActive, _id} = courseProps;
 
	//	3 Hooks in React
	//1. useState
	//2. useEffect
	//3. useContext 

	//Use the useState hook for the component to be able to store state
	//States are used to keep track of information related to individual components
	//Syntax: const [getter,setter] = useState(initialGetterValue); //get original value then set the new value

	// const [count,setCount] = useState(0);
	// const [seat, setSeat] = useState(5)

	// console.log(useState(0));

	/*function enroll(){
		
		if(seat > 0){
			
			setCount(count + 1);
			setSeat(seat - 1);
			console.log("Enrollees" + count);
		}else {
			alert("no more seats");
		}
	}*/



	return(

		(isActive) ?

		<Card className="mx-4">
	        <Card.Body>
	            <h1 className="text-center">{productName}</h1>
	            <Card.Subtitle>Description</Card.Subtitle>
	            <Card.Text>{description}</Card.Text>
	            <Card.Subtitle>Price:</Card.Subtitle>
	            <Card.Text>{price}</Card.Text>
	            
	            <Link className="btn btn-primary" to={`/courseView/${_id}`}>Details</Link>


	        </Card.Body>
	    </Card>

	    :

	    <Card className="mx-4" hidden>
	        <Card.Body>
	            <Card.Title>{productName}</Card.Title>
	            <Card.Subtitle>Description</Card.Subtitle>
	            <Card.Text>{description}</Card.Text>
	            <Card.Subtitle>Price:</Card.Subtitle>
	            <Card.Text>{price}</Card.Text>
	            
	            <Link className="btn btn-primary" to={`/courseView/${_id}`}>Details</Link>


	        </Card.Body>
	    </Card>

		)
}

